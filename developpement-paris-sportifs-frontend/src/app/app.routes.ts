import { Routes } from '@angular/router';
import { HomeModule } from './pages/home/home.module';


export const routes: Routes = [

{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {path:'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomeModule) }


]
