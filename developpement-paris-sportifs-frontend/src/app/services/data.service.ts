import { Injectable } from '@angular/core';
import { HttpClient , HttpParams  } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiUrl = 'http://localhost:3000/api'; // Replace with your API endpoint

  constructor(private http: HttpClient) { }

    // Example: GET request
    getData(league : any  , page : number ): Observable<any> {

      const url = `${this.apiUrl}/teams`; 
      // Use HttpParams to handle query parameters
      const params = new HttpParams().set('search', league).set('page', page.toString());
  
      return this.http.get<any>(url, { params });


    }
     
    
}
