import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { StoreService } from '../../services/store.service';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
})
export class HomeComponent implements OnInit {

  searchTerm: any;
  messageError: any;
  teamList: any;
  searchResults: string[] = [];
  currentPage: number = 1;

  constructor(private router: Router, private dataService: DataService , private  storeService: StoreService) {}

  ngOnInit(): void {
  }
  getPlayersByTeam(team: any) {
    
    this.storeService.sendData(team);
    this.router.navigate(['/home/players']);

  }


  getData(event: any): void {
    this.searchTerm = event.value;
    if (this.searchTerm.trim() === '') {
      this.currentPage=1}
    this.dataService.getData(event?.value, this.currentPage).pipe(
      map((leagueData) => leagueData.data),
      catchError((error) => {
        // Handle error
        this.handleLeagueNotFound();
        console.error('Error fetching league data:', error);
        return of([]);
      })
    ).subscribe((data) => {
      if (!data || !data.teams || data.teams.length === 0) {
        this.handleLeagueNotFound();
        return;
      }
  
      if (this.currentPage === 1) {
        this.teamList = data.teams;
      } else {
        // add new data to the existing array
        this.teamList = this.teamList.concat(data.teams.filter((newTeam: any) => 
          !this.teamList.some((existingTeam: any) => existingTeam._id === newTeam._id))
        );
      }
  
      // Check if there are more pages
      if (this.currentPage < data.totalPages) {
        this.currentPage++;
      }
     

    });
  }




 
  private handleLeagueNotFound(): void {
    this.teamList = [];
    this.messageError = 'League not found';
  }
}
