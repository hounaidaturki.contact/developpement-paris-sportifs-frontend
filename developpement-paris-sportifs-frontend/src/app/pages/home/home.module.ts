import { NgModule ,  } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule ,MatListItem } from '@angular/material/list';
import { MatCardImage } from '@angular/material/card';
import { PlayersComponent } from './players/players.component';
import { MatIconModule } from '@angular/material/icon';
import { DatePipe , CurrencyPipe } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [HomeComponent ,PlayersComponent],
  imports: [
    MatButtonModule ,
    HttpClientModule ,
    MatToolbarModule,
    MatGridListModule , 
    DatePipe,
    CurrencyPipe,
        MatListModule,
    FormsModule,
    MatCardModule , 
     MatFormFieldModule,
    MatInputModule,
 MatIconModule ,
    HomeRoutingModule
  ],exports:[ 
    MatToolbarModule , 
    MatCardImage,
    MatListModule,
    MatListItem , 
    MatCardModule , 
    MatFormFieldModule,
   MatInputModule
  ]
  
})
export class HomeModule { }
