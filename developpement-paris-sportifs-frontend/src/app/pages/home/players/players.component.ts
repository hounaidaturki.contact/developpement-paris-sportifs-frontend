import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'; 
import { StoreService } from '../../../services/store.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrl: './players.component.css'
})
export class PlayersComponent implements OnInit {
  extraData: any;
  teamName : any ; 
  playerList : any ; 

  constructor( private location: Location , private storeService: StoreService ) {



  }


 
ngOnInit(): void {
 
  this.storeService.data$.subscribe((data:any) => {
    this.playerList = data.players ; 
    this.teamName = data.name ;
  });


  
}

goBack() {
  this.location.back(); 
}
}
